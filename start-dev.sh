#!/usr/bin/env bash

echo "Starting wild application.."
echo "We need a git username and password to fetch the configuration"

read -p "enter your git username: " GIT_USERNAME
read -s -p "enter your git password: " GIT_PASSWORD

export GIT_USERNAME
export GIT_PASSWORD

docker-compose -f docker-compose.yml -f docker-compose-dev.yml up -d