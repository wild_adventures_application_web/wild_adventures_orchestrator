# wild-adventure-orchestrator

## Configuration

  * Ensure that every port are configured correctly in the .env file
  * The API_HOST variable is used by the web-app do locate zuul-server
  * For the security reasons the web-app is not on the same network as the other microservices

### Start-up

  * Either use the start.sh or the ```docker-compose up -d```  command
  * You can now access the application on the port ```$WEB_APP_PORT```

### Start-up dev mode

  * In dev configuration every container has the port open
  * Use start-dev.sh or the ```docker-compose -f ./docker-compose.yml -f docker-compsoe-dev.yml up -d```
